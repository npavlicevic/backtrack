import backtrack;
import core.stdc.stdio;
import core.stdc.stdlib;
import core.stdc.string;

void main() {
  int colsn;
  fscanf(stdin, "%d", &colsn);
  int *cols = cast(int*)calloc(colsn, int.sizeof);
  array_rand(cols, colsn);
  array_print(cols, colsn);
  int k;
  fscanf(stdin, "%d", &k);
  int *chosen = cast(int*)calloc(k, int.sizeof);
  combinations_repeat(chosen, k, 0, cols, colsn, 0);
  free(cols);
  free(chosen);
}
