#
# Makefile 
# backtrack (dfs) algorithms 
#

CC=dmd
CFLAGS=-H -gc

FILES=backtrack/backtrack.d main.d
FILES_PERMUTATION=backtrack/backtrack.d main_permutation.d
FILES_COMBINATION=backtrack/backtrack.d main_combination.d
FILES_COMBINATION_REPEAT=backtrack/backtrack.d main_combination_repeat.d
CLEAN=main main_permutation main_combination main_combination_repeat

all: main main_permutation main_combination main_combination_repeat

main: ${FILES}
	${CC} $^ -ofmain ${CFLAGS}

main_permutation: ${FILES_PERMUTATION}
	${CC} $^ -ofmain_permutation ${CFLAGS}

main_combination: ${FILES_COMBINATION}
	${CC} $^ -ofmain_combination ${CFLAGS}

main_combination_repeat: ${FILES_COMBINATION_REPEAT}
	${CC} $^ -ofmain_combination_repeat ${CFLAGS}

clean: ${CLEAN}
	rm $^
