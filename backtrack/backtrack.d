module backtrack;

import std.math;
import std.random;
import core.stdc.stdio;

bool nqueens_possible(int *cols, int col) {
  for(int i = 0; i < col; i++) {
    if(cols[i] == cols[col]) {
      return false;
    }
    if((col - i) == abs(cols[i] - cols[col])) {
      return false;
    }
  }
  return true;
}
void nqueens_solve(int *cols, int col, int colsn) {
  if(col >= colsn) {
    nqueens_print(cols, colsn);
  } else {
    for(int i = 0; i < colsn; i++) {
      cols[col] = i;
      if(nqueens_possible(cols, col)) {
        nqueens_solve(cols, col + 1, colsn);
      }
    }
  }
}
void nqueens_print(int *cols, int colsn) {
  for(int i = 0; i < colsn; i++) {
    for(int j = 0; j < colsn; j++) {
      if(cols[j] == i) {
        printf("Q ");
      } else {
        printf("+ ");
      }
    }
    printf("\n");
  }
  printf("\n");
}

void permutations(int *chosen, int choice, int n) {
  if(choice >= n) {
    array_print(chosen, n);
  } else {
    for(int i = choice; i < n; i++) {
      array_swap(chosen, choice, i);
      permutations(chosen, choice + 1, n);
      array_swap(chosen, choice, i);
    }
  }
}

void combinations(int *chosen, int k, int choice, int *choices, int n, int from) {
  if(choice >= k) {
    array_print(chosen, k);
  } else {
    for(int i = from; i < n; i++) {
      chosen[choice] = choices[i];
      combinations(chosen, k, choice + 1, choices, n, i + 1);
    }
  }
}

void combinations_repeat(int *chosen, int k, int choice, int *choices, int n, int from) {
  if(choice >= k) {
    array_print(chosen, choice);
  } else {
    for(int i = from; i < n; i++) {
      chosen[choice] = choices[i];
      combinations_repeat(chosen, k, choice + 1, choices, n, i);
    }
  }
}

void array_print(int *cols, int colsn) {
  int i = 0;
  for(; i < colsn; i++) {
    printf("%d ", cols[i]);
  }
  printf("\n");
}

void array_swap(int *cols, int from, int to) {
  int temp = cols[to];
  cols[to] = cols[from];
  cols[from] = temp;
}

void array_rand(int *cols, int colsn, int max = 32) {
  int i = 0;
  for(; i < colsn; i++) {
    cols[i] = uniform(1, max);
  }
}

// N queens
// ========
// General rule
// ````````````
// use backtracking to solve for(i=0; i<colsn; i++) {cols[col]=i; solve(cols, col+1, colsn);}
// cols[col] == cols[i]
// abs(cols[col] - cols[i]) == col - i ~ difference in cols same as difference in rows ~
// if above not safe
// if true do not place figure same column is shared
//
// General description
// ```````````````````
// place n queens on n x n chess board
//
// Backtracking
// ````````````
// general algorithm
//
// backtrack(v)
//  foreach(child u of v)
//    if(promising(u))
//      if(solution_at(u))
//        print u
//      else
//        backtrack(u)
//
// It is pruned depth first search. Depth first search expands all nodes.
//
// general algorithm
//
// dfs(v)
//  foreach(child u of v)
//    dfs(u)
//
// Complexity ~ no pruning
// ```````````````````````
// (n`2 n) all cells ~ not any contraints
// n`n ~ row contraint
// n! ~ row & column constaint
//
// Complexity
// ``````````
// 1+n+n`2+n`3+n`4+...+n`n=(n`(n+1))-1 / n-1
//
// Example 4x4
// ```````````
// +Q++
// +++Q
// Q+++
// ++Q+

// Permutations
// ========
// General rule
// ````````````
// no computation involved, permute existing array of elements
// swap ~> call ~> swap
//
// General description
// ```````````````````
// find possible permutations on array of elements
//
// Complexity
// ``````````
// n!

// Combinations
// ========
// General rule
// ````````````
// no computation involved, use existing array
// add choice ~> push to stack next counter (i+1) ~> backtrace
//
// General description
// ```````````````````
// find possible combinations on array of element
//
// Complexity
// ``````````
// (n k)

// Combinations including repetitions
// ========
// General rule
// ````````````
// no computation involved, use existing array
// add choice ~> push to stack next counter (i) ~> backtrace
//
// General description
// ```````````````````
// find possible combinations on array of element including repetitions
//
// Complexity
// ``````````
// (n+k-1 k)
