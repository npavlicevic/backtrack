import backtrack;
import core.stdc.stdio;
import core.stdc.stdlib;
import core.stdc.string;

void main() {
  int colsn;
  fscanf(stdin, "%d", &colsn);
  int *cols = cast(int*)calloc(colsn, int.sizeof);
  memset(cols, -1, colsn);
  nqueens_solve(cols, 0, colsn);
  free(cols);
}
